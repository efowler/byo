//
//  SearchViewController.m
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "SearchViewController.h"
#import "Artist.h"
#import "ArtistService.h"
#import "ArtistDetailViewController.h"

@interface SearchViewController () <UITextFieldDelegate>

@property (nonatomic, strong) NSArray* artists;
@property (weak, nonatomic) IBOutlet UITextField *artistSearchTextfield;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBar.translucent = NO;
    _artistSearchTextfield.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_artistSearchTextfield becomeFirstResponder];
}

#pragma mark - UITextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    ArtistService* artistService = [ArtistService new];
    __weak typeof(self) weakSelf = self;
    [artistService searchForArtist:textField.text completion:^(ArtistSearchResponse *response) {
        weakSelf.artists = response.artists;
        [weakSelf showArtistDetailsForArtist];
    } error:^(NSInteger statusCode, NSError *error) {
        UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"Attention" message:@"No results for that search. Check for spelling errors and try again" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }];
    
    return NO;
}

-(void)showArtistDetailsForArtist {
    Artist* artist = (Artist*)_artists[0];
    ArtistDetailViewController* artistDetailVC = [[ArtistDetailViewController alloc]
                                                  initWithNibName:NSStringFromClass([ArtistDetailViewController class]) bundle:nil];
    artistDetailVC.artistName = artist.name;
    [self.navigationController pushViewController:artistDetailVC animated:YES];
    
}

@end
