//
//  ArtistDetailViewController.m
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "ArtistDetailViewController.h"
#import "Artist.h"
#import "ArtistService.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageViewAligned.h"

@interface ArtistDetailViewController ()

@property (nonatomic, weak) IBOutlet UIImageViewAligned* artistImageView;
@property (weak, nonatomic) IBOutlet UILabel *bandNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bandLocationLabel;

@property (weak, nonatomic) IBOutlet UILabel *numListensLabel;
@property (weak, nonatomic) IBOutlet UILabel *numListenersLabel;
@property (weak, nonatomic) IBOutlet UILabel *numBandMembersLabel;

@property (nonatomic, weak) IBOutlet UIWebView* bioWebView;
@property (nonatomic, strong) Artist* artist;

@end

@implementation ArtistDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _artistName;
    
    ArtistService* artistService = [ArtistService new];
    _artist = nil;
    [artistService getArtistInfoForArtistName:_artistName withCompletion:^(ArtistInfoResponse* response) {
        _artist = response.artist;
        [self loadArtistDataOntoView];
    } error:^(NSInteger statusCode, NSError *error) {
        NSLog(@"Failure.");
    }];
    
    self.navigationController.navigationBar.translucent = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.bioWebView.scrollView.bounces = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!_artist) {
        _bandNameLabel.text = @"---";
        _bandLocationLabel.text = @"---";
        
        _numBandMembersLabel.text = @"---";
        _numListenersLabel.text = @"---";
        _numListensLabel.text = @"---";
    }
}

-(void)loadBlankDefaults {
    
}

- (void)loadArtistDataOntoView {
    // First load the webview with the HTML content
    // Assume it has been sanitized by the web service
    [_bioWebView loadHTMLString:_artist.bio.summary baseURL:nil];
    
    // Grab an image of the artist and display
    // Let's grab the second biggest and rename it to
    // give us the largest because it looks like the
    // largest doesn't work with mustachify in some
    // cases (taylor swift)
    Image* image = (Image*)_artist.images[1];
    NSString* url = image.url;
    NSString* smallerSize = @"252";
    NSRange rSmallerSize = [image.url rangeOfString:smallerSize];
    if (NSNotFound != rSmallerSize.location) {
        url = [image.url stringByReplacingCharactersInRange:rSmallerSize withString:@"500"];
    }
    NSString* imageUrl = [NSString stringWithFormat:@"http://mustachify.me/?src=%@", url];
    // Use AFNetworking's handy category to load the image from the web service in
    [_artistImageView setImageWithURL:[NSURL URLWithString:imageUrl]
                     placeholderImage:[UIImage imageNamed:@"placeholder-image"]];
    _artistImageView.contentMode = UIViewContentModeScaleAspectFill;
    // Use the handy 3rd party library to align the image to the top
    // since most images are of a person, we want to see their face
    _artistImageView.alignTop = YES;
    
    _bandNameLabel.text = _artist.name;
    
    if (_artist.bio && [_artist.bio.placeFormed rangeOfString:@","].location != NSNotFound) {
        _bandLocationLabel.text = [_artist.bio.placeFormed substringToIndex:[_artist.bio.placeFormed rangeOfString:@","].location];
    } else if(!_artist.bio) {
        _bandLocationLabel.text = @"Anytown, USA";
    } else {
        _bandLocationLabel.text = _artist.bio.placeFormed;
    }
    
    _numBandMembersLabel.text = [NSString stringWithFormat:@"%lu", _artist.bandMembers.members.count];
    if ([_numBandMembersLabel.text isEqualToString:@"0"]) {
        // Let's at least give them 1 member so we don't make Eliot look dumb
        _numBandMembersLabel.text = @"1";
    }
    
    _numListenersLabel.text = [self abbreviateNumber:[_artist.stats.listeners intValue]];
    _numListensLabel.text = [self abbreviateNumber:[_artist.stats.playcount intValue]];
}

// Found this guy on stack overflow... love that site
-(NSString *)abbreviateNumber:(int)num {
    
    NSString *abbrevNum;
    float number = (float)num;
    
    //Prevent numbers smaller than 1000 to return NULL
    if (num >= 1000) {
        NSArray *abbrev = @[@"K", @"M", @"B"];
        
        for (int i = (int)abbrev.count - 1; i >= 0; i--) {
            
            // Convert array index to "1000", "1000000", etc
            int size = pow(10,(i+1)*3);
            
            if(size <= number) {
                // Removed the round and dec to make sure small numbers are included like: 1.1K instead of 1K
                number = number/size;
                NSString *numberString = [self floatToString:number];
                
                // Add the letter for the abbreviation
                abbrevNum = [NSString stringWithFormat:@"%@%@", numberString, [abbrev objectAtIndex:i]];
            }
            
        }
    } else {
        
        // Numbers like: 999 returns 999 instead of NULL
        abbrevNum = [NSString stringWithFormat:@"%d", (int)number];
    }
    
    return abbrevNum;
}

- (NSString *) floatToString:(float) val {
    NSString *ret = [NSString stringWithFormat:@"%.1f", val];
    unichar c = [ret characterAtIndex:[ret length] - 1];
    
    while (c == 48) { // 0
        ret = [ret substringToIndex:[ret length] - 1];
        c = [ret characterAtIndex:[ret length] - 1];
        
        //After finding the "." we know that everything left is the decimal number, so get a substring excluding the "."
        if(c == 46) { // .
            ret = [ret substringToIndex:[ret length] - 1];
        }
    }
    
    return ret;
}

@end
