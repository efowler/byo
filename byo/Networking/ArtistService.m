//
//  ArtistService.m
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "ArtistService.h"

NSString *const GET_ARTIST_INFO_METHOD = @"artist.getinfo";
NSString *const SEARCH_ARTIST_METHOD = @"artist.search";
NSString *const ARTIST_QUERY_PARAMETER_NAME = @"artist";

@implementation ArtistService

-(void)searchForArtist:(NSString *)artistName
            completion:(ArtistSearchResponseBlock)responseBlock
                 error:(NetworkErrorBlock)errorBlock {
    NSDictionary* params = @{ARTIST_QUERY_PARAMETER_NAME: artistName};
    
    NSError* error;
    AFHTTPRequestOperation *operation = [self createLastFMGetRequestOperationWithApiMethod:SEARCH_ARTIST_METHOD parameters:params error:&error];
    if (error) {
        // Internal Error
        errorBlock(499, error);
    }
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSError* deserializationError;
            ArtistSearchResponse* response = [[ArtistSearchResponse alloc] initWithDictionary:responseObject[@"results"][@"artistmatches"] error:&deserializationError];
            if (deserializationError) {
                errorBlock(498, error);
            } else {
                responseBlock(response);
            }
        } else {
            // Should make a special error domain for internal state errors
            // omitting for brevity
            errorBlock(497, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(496, error);
    }];
    
    [operation start];
}

-(void)getArtistInfoForArtistName:(NSString *)artistName
                   withCompletion:(ArtistInfoResponseBlock)responseBlock
                        error:(NetworkErrorBlock)errorBlock {
    NSDictionary* params = @{ARTIST_QUERY_PARAMETER_NAME: artistName};
    
    NSError* error;
    AFHTTPRequestOperation *operation = [self createLastFMGetRequestOperationWithApiMethod:GET_ARTIST_INFO_METHOD parameters:params error:&error];
    if (error) {
        // Internal Error
        errorBlock(499, error);
    }
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSError* deserializationError;
            ArtistInfoResponse* response = [[ArtistInfoResponse alloc] initWithDictionary:responseObject error:&deserializationError];
            if (deserializationError) {
                errorBlock(498, error);
            } else {
                responseBlock(response);
            }
        } else {
            errorBlock(497, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(496, error);
    }];
    
    [operation start];
}

@end
