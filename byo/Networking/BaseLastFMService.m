//
//  BaseLastFMService.m
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "BaseLastFMService.h"
#import "AppConstants.h"

@implementation BaseLastFMService

-(AFHTTPRequestOperation*)createLastFMGetRequestOperationWithApiMethod:(NSString*)apiMethod parameters:(NSDictionary *)params error:(NSError *__autoreleasing *)error{
    NSDictionary* baseParams = @{API_KEY_QUERY_PARAMETER_NAME: API_KEY_QUERY_PARAMETER_VALUE,
                                 FORMAT_QUERY_PARAMETER_NAME: FORMAT_QUERY_PARAMETER_VALUE,
                                 METHOD_QUERY_PARAMETER_NAME: apiMethod};
    NSMutableDictionary* fullParams = [NSMutableDictionary dictionaryWithDictionary:baseParams];
    [fullParams addEntriesFromDictionary:params];
    
    AFHTTPRequestSerializer* serializer = [AFHTTPRequestSerializer serializer];
    NSURLRequest *request = [serializer requestWithMethod:HTTP_GET URLString:BASE_LAST_FM_SERVICE_URL parameters:fullParams error:error];
    
    if (*error) {
        return nil;
    }
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    return operation;
}

@end
