//
//  BaseLastFMService.h
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "JsonModel.h"

typedef void(^NetworkErrorBlock)(NSInteger statusCode, NSError *error);

@interface BaseLastFMService : NSObject

-(AFHTTPRequestOperation*)createLastFMGetRequestOperationWithApiMethod:(NSString*)apiMethod parameters:(NSDictionary*)params error:(NSError *__autoreleasing *)error;

@end
