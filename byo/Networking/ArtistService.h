//
//  ArtistService.h
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "BaseLastFMService.h"
#import "ArtistInfoResponse.h"
#import "ArtistSearchResponse.h"

typedef void (^ArtistInfoResponseBlock)(ArtistInfoResponse* response);
typedef void (^ArtistSearchResponseBlock)(ArtistSearchResponse* response);

@interface ArtistService : BaseLastFMService

-(void)searchForArtist:(NSString*)artistName
            completion:(ArtistSearchResponseBlock)responseBlock
                 error:(NetworkErrorBlock)errorBlock;

-(void)getArtistInfoForArtistName:(NSString*)artistName
                   withCompletion:(ArtistInfoResponseBlock)responseBlock
                        error:(NetworkErrorBlock)errorBlock;

@end
