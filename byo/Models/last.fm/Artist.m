//
//  Artist.m
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "Artist.h"

@implementation Artist

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"ontour": @"onTour",
                                                       @"bandmembers": @"bandMembers",
                                                       @"image": @"images"
                                                       }];
}

-(instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err {
    self = [super initWithDictionary:dict error:err];
    
    if (self) {
        NSArray* sizeOrder = @[@"mega", @"extralarge", @"large", @"medium", @"small"];
        
        if (self.images && self.images.count > 1) {
            NSArray* sortedImages = [self.images sortedArrayUsingComparator:^NSComparisonResult(Image* image1, Image* image2) {
                return [sizeOrder indexOfObject:image1.size] > [sizeOrder indexOfObject:image2.size];
            }];
            self.images = (NSArray<Image>*)sortedImages;
        }
    }
    
    return self;
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
