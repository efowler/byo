//
//  BandMember.h
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "JSONModel.h"
#import "Member.h"

@protocol BandMember
@end

@interface BandMember : JSONModel

@property (nonatomic, strong) NSArray<Member>* members;

@end
