//
//  ArtistSearchResponse.h
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "JSONModel.h"
#import "Artist.h"

@interface ArtistSearchResponse : JSONModel

@property (nonatomic, strong) NSArray<Artist>* artists;

@end
