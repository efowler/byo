//
//  ArtistInfoResponse.h
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "JSONModel.h"
#import "Artist.h"

@interface ArtistInfoResponse : JSONModel

@property (nonatomic, strong) Artist* artist;

@end
