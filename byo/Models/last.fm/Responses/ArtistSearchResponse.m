//
//  ArtistSearchResponse.m
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "ArtistSearchResponse.h"

@implementation ArtistSearchResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"artist": @"artists"
                                                       }];
}

@end
