//
//  Bio.m
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "Bio.h"

@implementation Bio

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"placeformed": @"placeFormed",
                                                       @"yearformed": @"yearFormed"
                                                       }];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
