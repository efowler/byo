//
//  Bio.h
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "JSONModel.h"

@protocol Bio
@end

@interface Bio : JSONModel

@property (nonatomic, strong) NSString* published;
@property (nonatomic, strong) NSString* summary;
@property (nonatomic, strong) NSString* content;
@property (nonatomic, strong) NSString<Optional>* placeFormed;
@property (nonatomic, strong) NSNumber* yearFormed;

@end
