//
//  Stats.h
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "JSONModel.h"

@protocol Stats
@end

@interface Stats : JSONModel

@property (nonatomic, strong) NSNumber* listeners;
@property (nonatomic, strong) NSNumber* playcount;

@end
