//
//  Image.h
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "JSONModel.h"

@protocol Image
@end

@interface Image : JSONModel

@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* size;

@end
