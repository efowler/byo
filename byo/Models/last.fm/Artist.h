//
//  Artist.h
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "JSONModel.h"
#import "Stats.h"
#import "Bio.h"
#import "BandMember.h"
#import "Image.h"

@protocol Artist
@end

@interface Artist : JSONModel

@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* mbid;
@property (nonatomic, strong) BandMember<Optional>* bandMembers;
@property (nonatomic, strong) NSArray<Image>* images;
@property (nonatomic, strong) NSString* streamable;
@property (nonatomic, strong) NSString<Optional>* onTour;
@property (nonatomic, strong) Stats<Optional>* stats;
//@property (nonatomic, strong) NSArray<Artist>* similar;
@property (nonatomic, strong) Bio<Optional>* bio;

@end
