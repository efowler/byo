//
//  AppConstants.h
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppConstants : NSObject

// Services
extern NSString *const BASE_LAST_FM_SERVICE_URL;

extern NSString *const API_KEY_QUERY_PARAMETER_NAME;
extern NSString *const API_KEY_QUERY_PARAMETER_VALUE;
extern NSString *const FORMAT_QUERY_PARAMETER_NAME;
extern NSString *const FORMAT_QUERY_PARAMETER_VALUE;
extern NSString *const METHOD_QUERY_PARAMETER_NAME;

extern NSString *const ACCEPT_HEADER_NAME;
extern NSString *const CONTENT_ACCEPT_TYPE;

extern NSString *const HTTP_GET;
extern NSString *const HTTP_POST;
extern NSString *const HTTP_PUT;

extern NSString *const GET_ARTIST_INFO_METHOD;
@end
