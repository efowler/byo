//
//  AppConstants.m
//  byo
//
//  Created by Eliot Fowler on 12/19/14.
//  Copyright (c) 2014 Eliot Fowler. All rights reserved.
//

#import "AppConstants.h"

@implementation AppConstants

// Services
// Base Urls
NSString *const BASE_LAST_FM_SERVICE_URL = @"http://ws.audioscrobbler.com/2.0";

// General Config
NSString *const ACCEPT_HEADER_NAME = @"Accept";
NSString *const CONTENT_ACCEPT_TYPE = @"application/json";

NSString *const HTTP_GET = @"GET";
NSString *const HTTP_POST = @"POST";
NSString *const HTTP_PUT = @"PUT";

// Query Parameters
NSString *const API_KEY_QUERY_PARAMETER_NAME = @"api_key";
NSString *const API_KEY_QUERY_PARAMETER_VALUE = @"ff6c19c8cbcf1e5ab8fcb42e8e9112ce";

NSString *const FORMAT_QUERY_PARAMETER_NAME = @"format";
NSString *const FORMAT_QUERY_PARAMETER_VALUE = @"json";

NSString *const METHOD_QUERY_PARAMETER_NAME = @"method";

@end
